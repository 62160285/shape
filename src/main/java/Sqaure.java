/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author akemi
 */
public class Sqaure extends Shape {

    private double side;

    public double getSide() {
        return side;
    }

    @Override
    public String toString() {
        return "Sqaure{" + "side=" + side + '}';
    }

    public void setSide(double side) {
        this.side = side;
    }

    public Sqaure(double side) {
        super("Squre");
        this.side = side;
    }

    @Override
    public double calArea() {
        return side * side;
    }

}
